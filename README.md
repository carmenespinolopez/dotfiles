# Install ZSH

```bash
sudo apt-get install zsh
```
# Install Oh my ZSH

```bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

# Install stow
Stow is a symlink farm manager which takes distinct sets of software and/or data located in separate directories on the filesystem, and makes them all appear to be installed in a single directory tree.

```bash
sudo apt-get install -y stow
```

# To configure ZSH

```bash
vim ~/.dotfiles/ZSH/.zshrc
```

# To install the fonts

```bash
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts
```

# To change the font 
Terminal preferences and choose the font: * Meslo LG M for Powerline Regular 12*

# To make ZSH the default shell

```bash
chsh -s /bin/zsh
```

# To install TMUX

```bash
sudo apt-get install tmux
```

# To use TMUX
Horizontal: CTRL + A + SHIFT 2 (")

Vertical: CTRL + A SHIFT 5 (%)
